﻿using System;

namespace PopulationTask
{
    public static class Population
    {
        public static int GetYears(int initialPopulation, double percent, int visitors, int currentPopulation)
        {
            if (initialPopulation <= 0)
            {
                throw new ArgumentException($"{initialPopulation} is less or equals 0.", nameof(initialPopulation));
            }

            if (visitors < 0)
            {
                throw new ArgumentException($"{visitors} is less 0.", nameof(visitors));
            }

            if (currentPopulation < initialPopulation)
            {
                throw new ArgumentException($"{currentPopulation} is less 0.", nameof(currentPopulation));
            }

            if (percent < 0 || percent > 100)
            {
                throw new ArgumentOutOfRangeException(nameof(percent), $"{percent} is less 0 or more 100.");
            }

            int countOfYears = 0;
            double doubleInitialPopulation = Convert.ToDouble(initialPopulation);

            while (doubleInitialPopulation < currentPopulation)
            {
                doubleInitialPopulation += doubleInitialPopulation * percent / 100;
                doubleInitialPopulation += visitors;
                countOfYears++;
            }

            return countOfYears;
        }
    }
}
